# Instead, install the most recent version of these packages.
pandas==0.24.2
pip scipy==1.3.0
tdigest==0.5.2.2
seaborn==0.9.0
scikit-learn==0.21.2
matplotlib==3.1.0
hurry.filesize==0.9
PyYAML==5.1.2
tqdm==4.36.1
zipp==0.6.0
urllib3==1.24.3
notebook==6.0.1